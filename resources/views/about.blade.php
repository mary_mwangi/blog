

<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        div.box {
    position: relative;
    width: 200px;
    height: 200px;
    background: #eee;
    color: #000;
    padding: 20px;    
}

div.box:hover {
    cursor: hand;
    cursor: pointer;
    opacity: .9;
}

a.divLink {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    text-decoration: none;
    /* Makes sure the link doesn't get underlined */
    z-index: 10;
    /* raises anchor tag above everything else in div */
    background-color: white;
    /*workaround to make clickable in IE */
    opacity: 0;
    /*workaround to make clickable in IE */
    filter: alpha(opacity=0);
    /*workaround to make clickable in IE */
}
    </style>
</head>
<body>
<div class="box">
    <h2>Box Title</h2>
    <p>The Quick Brown Fox Jumped Over The Lazy Dog</p>
    <p><a class="divLink" href="http://www.labnol.org/">Webpage URL</a></p>
</div>
<div class="box">
    <h2>Box Title</h2>
    <p>The Quick Brown Fox Jumped Over The Lazy Dog</p>
    <p><a class="divLink" href="http://www.labnol.org/">Webpage URL</a></p>
</div>
<div class="box">
    <h2>Box Title</h2>
    <p>The Quick Brown Fox Jumped Over The Lazy Dog</p>
    <p><a class="divLink" href="http://www.labnol.org/">Webpage URL</a></p>
</div>
</body>
</html>